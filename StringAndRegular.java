import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*� �����, ����� ������ ������� � ��� ������� ����� ������� ������ � 1 �����*/


public class Main {
	public static void main(String[] args){
		/*������ 1.1. � ���� �� ���������� ������� � ��������� ������, ��� � ����� �� ���- � �� �����, ������� ������ ���������� �����*/
		System.out.println("������ 1.1\n");
		
		
		String input = "Input text, with words, punctuation, etc. Well, it's rather short.";
		Pattern p = Pattern.compile("[\\w]+");
		Matcher m = p.matcher(input);

		while ( m.find() ) {
		    System.out.println(input.substring(m.start(), m.end()));
		}
//
//	
//
		/*������ 1.2.  � ������, ��� ������ �������� ������ ����� �� ���������� ����(�������\���������, �.�. ������ �����������) (it's -��� 2 ����� �.�. 's=is , ������ ���������� �� ��������*/
		System.out.println("\n\n\n///////\n������ 1.2\n");
		
		
		int minlen=input.length(),maxlen=0,n=0,k=0,j=0,i=0;
		for(i=0;i<input.length();i++){
			if ((input.charAt(i)>='a')&&(input.charAt(i)<='z')||(input.charAt(i)>='A')&&(input.charAt(i)<='Z'))	j++;
			else if (j!=0){
				if (j>maxlen){
					maxlen=j;
					n=i-j;
				}
				if (j<minlen){
					minlen=j;
					k=i-j;
				}
				j=0;
			}
		}
		i--;
		if ((input.charAt(i)>='a')&&(input.charAt(i)<='z')||(input.charAt(i)>='A')&&(input.charAt(i)<='Z'))	j++;
		else if (j!=0){
			if (j>maxlen){
				maxlen=j;
				n=i-j;
			}
			if (j<minlen){
				minlen=j;
				k=i-j;
			}
		}
		System.out.println("����� ������������ �����, ����� ��� = "+maxlen+" ��������");
		for(i=n;i<maxlen+n;i++) System.out.print(input.charAt(i));
		System.out.println("\n");
		System.out.println("����� ����������� �����, ����� ��� = "+minlen+" ��������");
		for(i=k;i<minlen+k;i++) System.out.print(input.charAt(i));
//		
//
//
		/*������ 1.3. ����� ������, � �� �����, ��� � ������ ���������. ������ ��� ����� � �����.*/
		System.out.println("\n\n\n///////\n������ 1.3\n");
		
		
		String inp = "+7 (3412) 517-647    8 (3412) 4997-12    +7 3412 90-41-90";
		System.out.println(inp.replace("+7 (3412)", "").replace("7 (3412)","").replace("+7 3412","").replace("7 3412","").replace("+8 (3412)", "").replace("8 (3412)","").replace("+8 3412","").replace("8 3412",""));
//
//
//
		/*������ 1.4.*/
		System.out.println("\n\n\n///////\n������ 1.4\n");
		
		
		String shablon = "���������, $userName, �������� ��� � ���, ��� �� ����� ����� $���������� ��������� �����, ����������� ��������� $������������ ������� ����������� ������ ��������. ������ ���������� ���������. ��������, �� ����������� ��������� ����������. � ���������, $��������������� $���������������������";
		String templeKey="����������";
		String templeValue="27111997";
		/*��� ������������ ������ ��� 1 ���, ��������� ������ � ������*/
		//String templeKey="userName";
		//String templeValue="�������";
		System.out.println(shablon.replace('$'+templeKey,templeValue));
	}
}
