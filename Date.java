import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Main {
	public static  void main(String[] args){
		/*������ 2.1. � �� ����� ������� ������� ��� ��������� ��� ��� � ��������, � ��������� �� �� ���� � �����, ��� ���� ����.*/
		System.out.println("������ 2.1\n");
		LocalDateTime today = LocalDateTime.now(); 
		System.out.println("Today's Local date : " + today); 
		System.out.println(today.getYear()+"-"+today.getMonthValue()+"-"+today.getDayOfMonth()+"   "
		+today.getHour()+":"+today.getMinute()+":"+today.getSecond());
		
		LocalDateTime hb=LocalDateTime.of(1997,11,27,12,0,0);
		System.out.println("My Happy Birthday : " + hb);
		System.out.println(hb.getYear()+"-"+hb.getMonthValue()+"-"+hb.getDayOfMonth()+"   "
		+hb.getHour()+":"+hb.getMinute()+":"+hb.getSecond());
		
		int year,month = 0,day,hour,minute,second;
		second=today.getSecond()-hb.getSecond();
		minute=today.getMinute()-hb.getMinute();
		hour=today.getHour()-hb.getHour();
		day=today.getDayOfYear()-hb.getDayOfYear();
		year=today.getYear()-hb.getYear();
		
		if (second<0){
			minute--;
			second+=60;
		}
		if (minute<0){
			hour--;
			minute+=60;
		}
		if (hour<0){
			day--;
			hour+=24;
		}
		if (day<0){
			if (today.getYear()-today.getYear()/4==0) day+=366;
			else day+=365;
			year--;
		}
		if (today.getYear()-today.getYear()/4==0){
			if (day>31)
				if (day>60)
					if (day>91)
						if (day>121)
							if (day>152)
								if (day>182)
									if (day>213)
										if (day>244)
											if (day>274)
												if (day>305)
													if (day>335) {month=11;day-=335;}
													else {month=10;day-=305;}
												else {month=9;day-=274;}
											else {month=8;day-=244;}
										else {month=7;day-=213;}
									else {month=6;day-=182;}
								else {month=5;day-=152;}
							else {month=4;day-=121;}
						else {month=3;day-=91;}
					else {month=2;day-=60;}
				else {month=1;day-=31;}
		}
		else{
			if (day>31)
				if (day>59)
					if (day>90)
						if (day>120)
							if (day>151)
								if (day>181)
									if (day>212)
										if (day>243)
											if (day>273)
												if (day>304)
													if (day>334){month=11;day-=334;}
													else {month=10;day-=304;}
												else {month=9;day-=273;}
											else {month=8;day-=243;}
										else {month=7;day-=212;}
									else {month=6;day-=181;}
								else {month=5;day-=151;}
							else {month=4;day-=120;}
						else {month=3;day-=90;}
					else {month=2;day-=59;}
				else {month=1;day-=31;}		
		}
		
		System.out.println("I leave :  " + year+" �����   " +month+" �������   "+day+" ����   "
		+hour+" �����   "+minute+" �����   "+second+" ������");
//		
//		
//		
	/*������ 2.2  ����� ���� ��������.*/
		System.out.println("\n\n\n///////\n������ 2.2\n");
		
		String d1="29.11.2016";
		String d2="27.11.1997";
		int day1=0,day2=0,month1=0,month2=0,year1=0,year2=0;
		day1=(d1.charAt(0)-'0')*10+(d1.charAt(1)-'0');
		day2=(d2.charAt(0)-'0')*10+(d2.charAt(1)-'0');
		month1=(d1.charAt(3)-'0')*10+(d1.charAt(4)-'0');
		month2=(d2.charAt(3)-'0')*10+(d2.charAt(4)-'0');
		year1=(d1.charAt(6)-'0')*1000+(d1.charAt(7)-'0')*100+(d1.charAt(8)-'0')*10+(d1.charAt(9)-'0');
		year2=(d2.charAt(6)-'0')*1000+(d2.charAt(7)-'0')*100+(d2.charAt(8)-'0')*10+(d2.charAt(9)-'0');
		LocalDate date1=LocalDate.of(year1,month1,day1);
		LocalDate date2=LocalDate.of(year2,month2,day2);
		Period period=Period.between(date1, date2);
		System.out.println(Math.abs(period.getYears())+" �����   "+Math.abs(period.getMonths())+" �������   "+Math.abs(period.getDays())+" ����");
//
//
//
		/*������ 2.3*/
		System.out.println("\n\n\n///////\n������ 2.3\n");
		
		
		String str="28 ������� 2013";
		int y,d;
		d=(str.charAt(0)-'0')*10+(str.charAt(1)-'0');
		y=(str.charAt(str.length()-4)-'0')*1000+(str.charAt(str.length()-3)-'0')*100+(str.charAt(str.length()-2)-'0')*10+(str.charAt(str.length()-1)-'0');
		
		str=str.substring(3, str.length()-5);
		String s=str;
		s=s.substring(0,3);
		if (s=="���") s="Jan";
		else if (s.equals("���")) s="Feb";
		else if (s.equals("���")) s="Mar";
		else if (s.equals("���")) s="Apr";
		else if (s.equals("���")) s="May";
		else if (s.equals("���")) s="Jun";
		else if (s.equals("���")) s="Jul";
		else if (s.equals("���")) s="Aug";
		else if (s.equals("���")) s="Sep";
		else if (s.equals("���")) s="Oct";
		else if (s.equals("���")) s="Nov";
		else if (s.equals("���")) s="Dec";
		System.out.println(d+"/"+s+"/"+y);
	}
}
