import java.math.BigDecimal;
import java.util.Scanner;


public class Main {
	public static void main(String[] args){	
		/*������ 3.1 ���� ������� ����� �����, �� ����� ����� ������� Pi ���� �� �������� 5-7 ������ ����� �������, �� �������� 50 ������, ��� ����������� ����*/
		System.out.println("������ 3.1\n");
		BigDecimal toch;// ��������
		BigDecimal pi; //�������� ��
		int n; // ����� ������ ����
		BigDecimal chlen; // �������� ����� ����
		int r=2;
		BigDecimal KAPACb;
		
		toch=new BigDecimal(Math.pow(10,-3));
		System.out.println(toch.doubleValue());
		chlen = new BigDecimal(1);
		n = 1; 
		pi = new BigDecimal(0);
		while (toch.doubleValue() < chlen.abs().doubleValue()) {
            pi=pi.add(chlen);
            //KAPACb=new BigDecimal(-4/(2*n+1)+4/(2*n+3));
            chlen = new BigDecimal(Math.pow(-1, n)*(4.0/(2*n+1)));
           // System.out.println(chlen.doubleValue());
            n++;
        }
		KAPACb=new BigDecimal(3);
        pi=pi.add(KAPACb);
        System.out.println(pi);
//
//
//
        /*������ 3.1 ����� ������� �������, � ��� ������, � ���� �� ����� � ��������� 500 ������ �������� ������ ��� �� 1���, ������� � �������, ��� ���� � ���� � 50 �������, ��� ������ ������� ����� �������� �� ���.*/
		BigDecimal p=new BigDecimal(0);
		BigDecimal t=new BigDecimal(Math.pow(10, -500-(r*r/10)));
		BigDecimal cr=new BigDecimal(1);
		int k=1;
		while (t.doubleValue() < cr.abs().doubleValue()) {
            p=p.add(cr);           
            cr = new BigDecimal(Math.pow(-1, k)/(Math.pow(3, k)*(2*k+1.0))); 
            //System.out.println(cr);
            k++;
        }
		p=p.multiply(new BigDecimal(2.0*Math.sqrt(3.0)));
		System.out.println(p);
		System.out.println("S ����� � �������� "+r+" = "+p.multiply(new BigDecimal(r*r)));
        
//		
//		
//		
		/*������ 3.2*/
		System.out.println("\n\n\n///////\n������ 3.2\n");
		BigDecimal a=new BigDecimal(0.1),b=new BigDecimal(0.15),c=new BigDecimal(0.25);
		if (c.doubleValue()==a.doubleValue()+b.doubleValue()) System.out.println("��, ������ ������� �������� ������ ������ ����");
		else System.out.println("���, ������ ������� �� �������� ������ ������ ����");
	}	
}
